import React from 'react';
import { Button ,View, Text, StyleSheet} from 'react-native';
import { createStackNavigator } from 'react-navigation';

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return { title: 'Home', };};
  render() {
    return (
      <View style={ styles.container }>
        <Text>Home Screen</Text>
        <Button
            title="Go to about page"
            onPress={() => { this.props.navigation.navigate("About", {
              itemId: 86,
              otherParam: 'Anithing you want here',
            });
          }}
          >
        </Button>
        <Button
        title="Update title"
        onPress={() => this.props.navigation.setParams({otherParam : 'Mis à jour'})}
        >
        </Button>
      </View>
    );
  }
}

class AboutScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {title: navigation.getParam('otherParam', 'Anonyme screen'),};
  };
  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    const otherParam = navigation.getParam('otherParam', 'NO-ID');
    return (
      <View style={ styles.container }>
        <Text>About Screen</Text>
        <Text>itemId: {JSON.stringify(itemId)}</Text>
        <Text>otherParam: {JSON.stringify(otherParam)} </Text>
        <Button
          title = "About v2"
          onPress={() =>
            this.props.navigation.push('About', {
              itemId: Math.floor(Math.random() * 100),
            })}>
          </Button>
      </View>
    );
  }
}

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    About: {
      screen: AboutScreen,
    },
 },
 {
   initialRouteName: 'Home',
 }
);


const styles = StyleSheet.create({
      container : {
        flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#B2BED5'
    }
})

export default class App extends React.Component{
    render(){
      return <RootStack />;
    }
};
