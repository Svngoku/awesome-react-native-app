import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

class AboutScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>About Screen</Text>

        // <Button
        //     title="Go to home"
        //     onPress={() => this.props.navigation.navigate("Home")}>
        // </Button>

        <Button
            title="Go to about page"
            onPress={() => this.props.navigation.goBack()}>
      </View>
    );
  }
}

const RootStack = createStackNavigator (
    {
      Home: HomeScreen,
      About: AboutScreen,
    },
    {
      initialRouteName: 'Home',
    }
);
