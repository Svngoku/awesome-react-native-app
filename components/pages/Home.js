import React from 'react';
import { Button , View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

class HomeScreen extends React.Component {
  render() {
    return (
      <Container style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
            title="Go to about page"
            onPress={() => this.props.navigation.navigate("About")}>
        </Button>
      </Container>
    );
  }
}
